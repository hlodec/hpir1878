package agenda.test;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.IRepositoryActivity;
import agenda.model.repository.interfaces.IRepositoryContact;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class IntegrationTestBigBang {

	private IRepositoryActivity repAct;
	private IRepositoryContact repCon;

	@Before
	public void setup() {
		repCon = new RepositoryContactMock();
		repAct = new RepositoryActivityMock();
	}

	// testare unitara pentru modulul A: Adaugarea de Contacte
	@Test
	public void testUnitA() {
		int size1 = repCon.getContacts().size();
		try {
            String nume1 = "Mihai";
            String adresa1 = "Oxford Street";
            String telefon1 = "0787654321";

            repCon.addContact(nume1, adresa1, telefon1);
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int size1_2 = repCon.getContacts().size();
		if (size1_2 == size1 + 1)
			assertTrue(true);

        int size2 = repCon.getContacts().size();
        try {
            String nume2 = "";
            String adresa2 = "strada Viilor";
            String telefon2 = "0787654321";

            repCon.addContact(nume2, adresa2, telefon2);
        } catch (InvalidFormatException e) {
            assertTrue(true);
        }
        int size2_2 = repCon.getContacts().size();
        if (size2_2 == size1)
            assertTrue(true);
	}

    //	testare pentru modulul B: Adaugare Activitati
	@Test
	public void testUnitB() {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        try {
            Activity act1 = new Activity("aactivitate 1",
                    df.parse("08/20/2018 14:30"),
                    df.parse("08/20/2018 15:30"),
                    null,
                    "descriere 1");
            boolean result = repAct.addActivity(act1);
            assertTrue(result);
        } catch (ParseException e) {
            assertTrue(false);
        }

        try{
            for (Activity a : repAct.getActivities())
                repAct.removeActivity(a);

            Activity act2 = new Activity("activitate 2",
                    df.parse("08/20/2018 16:00"),
                    df.parse("08/20/2018 17:00"),
                    null,
                    "");
            boolean result = repAct.addActivity(act2);
            assertFalse(result);

            for (Activity a : repAct.getActivities())
                repAct.removeActivity(a);

            Activity act3 = new Activity("activitate 2",
                    df.parse("08/20/2018 16:00"),
                    df.parse("08/20/2018 17:00"),
                    null,
                    "descriere 2");
            repAct.addActivity(act3);

            Activity act4 = new Activity("activitate 3",
                    df.parse("08/20/2018 16:30"),
                    df.parse("08/20/2018 17:30"),
                    null,
                    "descriere 3");
            boolean result2 = repAct.addActivity(act4);
            assertFalse(result2);

            for (Activity a : repAct.getActivities())
                repAct.removeActivity(a);

            Activity act5 = new Activity("activitate 3",
                    df.parse("08/20/2018 16:30"),
                    df.parse("08/20/2018 17:30"),
                    null,
                    "description3");
            repAct.addActivity(act5);

            Activity act6 = new Activity("activitate 3",
                    df.parse("08/20/2018 16:30"),
                    df.parse("08/20/2018 17:30"),
                    null,
                    "descriere 3");

            boolean result3 = repAct.addActivity(act6);
            assertFalse(result3);
        }
        catch(Exception e){
            assertTrue(true);
        }

    }

    // testare pentru modulul C: afisare activitati dintr-o anumita data
	@Test
	public void testUnitC() {
        Calendar c = Calendar.getInstance();
        c.set(2018, 3 - 1, 20);
        List<Activity> result = repAct.activitiesOnDate(c.getTime());
        assertTrue(result.size() == 1);

        c.set('a', 3 - 1, 20);
        try {
            repAct.activitiesOnDate(c.getTime());
        } catch (Exception e) {
            assertTrue(true);
        }
	}

	@Test
	public void testIntegrareABC(){
        // add contact
        int size1 = repCon.getContacts().size();
        List<Contact> contacts = new ArrayList<Contact>();
        try {
            String nume1 = "Dana";
            String adresa1 = "strada Primaverii";
            String telefon1 = "0712345678";
            Contact contact = new Contact(nume1, adresa1, telefon1);
            repCon.addContact(nume1, adresa1, telefon1);
            contacts.add(contact);
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }
        int size1_2 = repCon.getContacts().size();
        if (size1_2 == size1 + 1)
            assertTrue(true);

        // add activitate
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        try {
            Activity act1 = new Activity("activity1",
                    df.parse("03/20/2018 15:30"),
                    df.parse("03/20/2018 16:30"),
                    contacts,
                    "description1");
            boolean result = repAct.addActivity(act1);
            assertTrue(result);
        } catch (ParseException e) {
            assertTrue(false);
        }

        // afisare activitati dintr-o anumita data
        Calendar c = Calendar.getInstance();
        c.set(2018, 3 - 1, 20);
        List<Activity> result = repAct.activitiesOnDate(c.getTime());
        assertTrue(result.size() == 2);
	}

}
