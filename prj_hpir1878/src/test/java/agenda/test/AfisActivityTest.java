package agenda.test;

import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityFile;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.classes.RepositoryContactFile;
import agenda.model.repository.interfaces.IRepositoryActivity;
import agenda.model.repository.interfaces.IRepositoryContact;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class AfisActivityTest {

	private IRepositoryActivity rep;

	@Before
	public void setUp() {
		rep = new RepositoryActivityMock();
	}

	@Test
	public void testCase1() {
		Calendar c = Calendar.getInstance();
		c.set(2018, 3 - 1, 20);
		List<Activity> result = rep.activitiesOnDate(c.getTime());
		assertTrue(result.size() == 1);
	}

	@Test
	public void testCase2() {
		Calendar c = Calendar.getInstance();
		c.set('a', 3 - 1, 20);
		try {
			rep.activitiesOnDate(c.getTime());
		} catch (Exception e) {
			assertTrue(true);
		}
	}
}
