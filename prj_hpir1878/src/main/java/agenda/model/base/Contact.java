package agenda.model.base;

import agenda.exceptions.InvalidFormatException;

public class Contact {
	private String Name;
	private String Address;
	private String Telefon;

	public Contact(){
		Name = "";
		Address = "";
		Telefon = "";
	}
	
	public Contact(String name, String address, String telefon) throws InvalidFormatException{
		if (!validTelefon(telefon)) throw new InvalidFormatException("Numar de telefon invalid!");
		if (!validName(name)) throw new InvalidFormatException("Nume invalid!");
		if (!validAddress(address)) throw new InvalidFormatException("Adresa invalida!");
		Name = name;
		Address = address;
		Telefon = telefon;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) throws InvalidFormatException {
		if (!validName(name)) throw new InvalidFormatException("Nume invalid!");
		Name = name;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) throws InvalidFormatException {
		if (!validAddress(address)) throw new InvalidFormatException("Adresa invalida!");
		Address = address;
	}

	public String getTelefon() {
		return Telefon;
	}

	public void setTelefon(String telefon) throws InvalidFormatException {
		if (!validTelefon(telefon)) throw new InvalidFormatException("Numar de telefon invalid!");
		Telefon = telefon;
	}

	public static Contact fromString(String str, String delim) throws InvalidFormatException
	{
		String[] s = str.split(delim);
		if (s.length!=3) throw new InvalidFormatException("Contact invalid!");
		if (!validTelefon(s[2])) throw new InvalidFormatException("Numar de telefon invalid!");
		if (!validName(s[0])) throw new InvalidFormatException("Nume invalid!");
		if (!validAddress(s[1])) throw new InvalidFormatException("Adresa invalida!");
		
		return new Contact(s[0], s[1], s[2]);
	}
	
	@Override
	public String toString() {
		return Name + "#" + Address + "#" + Telefon + "#";
	}
	
	private static boolean validName(String nume)
    {
		return nume.length() > 0 && nume.length() <= 255;
	}
	
	private static boolean validAddress(String adresa)
    {
		return adresa.length() > 0 && adresa.length() <= 255;
	}
	
	private static boolean validTelefon(String telefon)
    {
		return telefon.matches("^[0-9]*$") && telefon.length() == 10 && telefon.charAt(0) == '0' ;
	}
	
		
	@Override
	public boolean equals(Object obj) {
		if (! (obj instanceof Contact)) return false;
		Contact o = (Contact)obj;
		return Name.equals(o.Name) && Address.equals(o.Address) &&
				Telefon.equals(o.Telefon);
	}
	
}
