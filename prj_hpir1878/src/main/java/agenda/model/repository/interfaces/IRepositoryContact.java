package agenda.model.repository.interfaces;

import java.util.List;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Contact;

public interface IRepositoryContact {

	List<Contact> getContacts();
	void addContact(String nume, String adresa, String telefon)  throws InvalidFormatException;
	boolean removeContact(Contact contact);
	boolean saveContacts();
	int count();
	Contact getByName(String name);
}
