package agenda.startApp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import agenda.exceptions.InvalidFormatException;

import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.base.User;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.classes.RepositoryContactFile;
import agenda.model.repository.classes.RepositoryUserFile;
import agenda.model.repository.interfaces.IRepositoryActivity;
import agenda.model.repository.interfaces.IRepositoryContact;

public class MainClass {

	public static void main(String[] args) {
		BufferedReader in = null;
		try {
			RepositoryContactFile contactRep = new RepositoryContactFile();
			RepositoryUserFile userRep = new RepositoryUserFile();
			RepositoryActivityMock activityRep = new RepositoryActivityMock();

			User user = null;
			in = new BufferedReader(new InputStreamReader(System.in));
			while (user == null) {
				System.out.print("Enter username: ");
				String u = in.readLine();
				System.out.print("Enter password: ");
				String p = in.readLine();

				user = userRep.getByUsername(u);
				if (!user.isPassword(p))
					user = null;
			}

			int chosen = 0;
			while (chosen != 4) {
				printMenu();
				chosen = Integer.parseInt(in.readLine());
				try {
					switch (chosen) {
					case 1:
						adaugContact(contactRep, in);
						break;
					case 2:
						adaugActivitate(activityRep, in, user);
						break;
					case 3:
						afisActivitate(activityRep, in, user);
						break;
					}
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		System.out.println("Program over and out\n");
	}

	private static void afisActivitate(IRepositoryActivity activityRep, BufferedReader in, User user) {
		try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

			System.out.println("Afisare Activitate: \n");
			System.out.println("Data(format: mm/dd/yyyy): ");
			String dateS = in.readLine();
			Calendar c = Calendar.getInstance();
			c.set(Integer.parseInt(dateS.split("/")[2]), Integer.parseInt(dateS.split("/")[0]) - 1, Integer.parseInt(dateS.split("/")[1]));
			Date d = c.getTime();

			System.out.println("Activitatile din ziua " + dateS + ": ");

			List<Activity> act = activityRep.activitiesOnDate(d);
			for (Activity a : act) {
				System.out.println(dateFormat.format(a.getStart()) + " - " + dateFormat.format(a.getDuration()) +" : " + a.getDescription());
			}
		} catch (Exception e) {
			System.out.println("Eroare de citire: Data invalida!");
		}
	}

	private static void adaugActivitate(IRepositoryActivity activityRep, BufferedReader in, User user) {
		try {
			System.out.println("Adauga Activitate: \n");
			System.out.println("Descriere: ");
			String description = in.readLine();
			System.out.println("Start Date(format: mm/dd/yyyy): ");
			String dateS = in.readLine();
			System.out.println("Start Time(hh:mm): ");
			String timeS = in.readLine();
			Calendar c = Calendar.getInstance();
			c.set(Integer.parseInt(dateS.split("/")[2]),
					Integer.parseInt(dateS.split("/")[0]) - 1,
					Integer.parseInt(dateS.split("/")[1]),
					Integer.parseInt(timeS.split(":")[0]),
					Integer.parseInt(timeS.split(":")[1]));
			Date start = c.getTime();

			System.out.println("End Date(format: mm/dd/yyyy): ");
			String dateE = in.readLine();
			System.out.println("End Time(hh:mm): ");
			String timeE = in.readLine();
			
			c.set(Integer.parseInt(dateE.split("/")[2]),
					Integer.parseInt(dateE.split("/")[0]) - 1,
					Integer.parseInt(dateE.split("/")[1]),
					Integer.parseInt(timeE.split(":")[0]),
					Integer.parseInt(timeE.split(":")[1]));
			Date end = c.getTime();

			Activity act = new Activity(user.getName(), start, end, new LinkedList<Contact>(), description);

			activityRep.addActivity(act);

			System.out.println("S-a adugat cu succes\n");
		} catch (IOException e) {
			System.out.println("Eroare de citire: %s\n" + e.getMessage());
		}

	}

	private static void adaugContact(IRepositoryContact contactRep, BufferedReader in) {
		try {
			System.out.print("Adauga Contact: \n");
			System.out.print("Nume: ");
			String name = in.readLine();
			System.out.print("Adresa: ");
			String adress = in.readLine();
			System.out.print("Numar de telefon: ");
			String telefon = in.readLine();

			contactRep.addContact(name, adress, telefon);
			contactRep.saveContacts();

			System.out.print("Contact adaugat cu succes!\n");
		} catch (IOException e) {
			System.out.print("Eroare de citire:" + e.getMessage());
		} catch (InvalidFormatException e) {
			if (e.getCause() != null)
				System.out.print(e.getMessage());
			else
				System.out.print(e.getMessage());
		}

	}

	private static void printMenu() {
		System.out.println("\nPlease choose option:");
		System.out.println("1. Adauga contact");
		System.out.println("2. Adauga activitate");
		System.out.println("3. Afisare activitati din data de...");
		System.out.println("4. Exit");
		System.out.println("Alege: ");
	}
}
